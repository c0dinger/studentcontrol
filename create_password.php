<?php
require_once "db.php";
require_once "components/header.php";
$data = $_POST;
if (isset($data['do_signup']))
{
    $errors = array();
    if (trim($data['login']) == '')
    {
        $errors[] = 'Введите логин';
    }
    if ($data['password'] == '')
    {
        $errors[] = 'Введите пароль';
    }
    if (R::count("admin", "login = ?", array(
        $data['login']
    )) > 0)
    {
        $errors[] = "Пользователь с таким логином уже существует";
    }
    if (empty($errors))
    {
        $user = R::dispense('admin');
        $user->username = $data['login'];
        $user->password = $data['password'];
        $id = R::store($user);
        echo "<h5>Ваш аккаунт успешно создан!</h5>";
    }
    else
    {
        echo "<div class='text-danger'>" . $errors[0] . "</div>";
    }
}
if (isset($data['do_delete']))
{
    $user = R::dispense('admin');
    $id = $data['id'];
    R::exec("DELETE FROM `admin` WHERE `ID` = '{$id}'");
}

?>
<?php if (isset($_SESSION['logged_user']))
{ ?>
<div class="row">
    <div class="col">
        <form action="create_password.php" method="POST" class="shadow rounded-bottom pb-3 bg-primary pt-3">
            <div class="container">
                <h3 class="mt-5 text-light">Создать нового пользователя</h3>
                <div class="input-group mb-3 shadow">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="login">Имя</span>
                    </div>
                    <input type="text" class="form-control" placeholder="Имя пользователя" aria-label="login"
                        aria-describedby="login" name="login">
                </div>

                <div class="input-group mb-3 shadow">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="password">Пароль</span>
                    </div>
                    <input type="password" class="form-control" placeholder="Пароль" aria-label="password"
                        aria-describedby="password" name="password">
                </div>
                <button type="submit" class="btn btn-dark shadow" name="do_signup">Создать пароль</button>
            </div>
        </form>
    </div>
    <div class="col">
        <h3 class="mt-5">Удалить существующих пользователей</h3>
        <form method="POST" action="create_password.php">
            <div class="row">
                <div class="col">
                    <div class="card shadow" style="width: 18rem;">
                        <ul class="list-group list-group-flush">
                            <?php
    $users = R::getAssoc('SELECT id, username FROM `admin`');
    foreach ($users as $un => $admins)
    {
        echo "<li class='list-group-item'><strong>:$un: $admins</strong></li>";
    }
?>
                        </ul>
                    </div>
                </div>
                <div class="col">
                    <div class="input-group mb-3 shadow">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="password">ID</span>
                        </div>
                        <input type="text" class="form-control" placeholder="id" aria-label="id" aria-describedby="id"
                            name="id">
                    </div>
                    <button type="submit" class="btn btn-danger shadow" name="do_delete">Удалить</button>
                </div>
            </div>

        </form>

    </div>
</div>
<? require_once "components/exit-button.php"; ?>
<?php
}
else
{
    if (isset($data['do_login']))
    {
        $error = array();
        $user = R::findOne('admin', 'username = ?', array(
            $data['login']
        ));
        if ($user)
        {
            if ($data['password'] = $user->password)
            {
                $_SESSION['logged_user'] = $user;
            }
            else
            {
                $error[] = "Пароль введён не верно!";
            }
        }
        else
        {
            $error[] = "Данного логина не существует";
        }
    }
    if (!empty($error))
    {
        echo $error[0];
    }
?>

<form action="index.php" method="POST" class="shadow rounded-bottom pb-3 bg-primary pt-3">
    <div class="container">
        <h3 class="mt-5 text-light">Вход в StudentControl</h3>
        <div class="input-group mb-3 shadow">
            <div class="input-group-prepend">
                <span class="input-group-text" id="login">Имя</span>
            </div>
            <input type="text" class="form-control" placeholder="Имя пользователя" aria-label="login"
                aria-describedby="login" name="login">
        </div>

        <div class="input-group mb-3 shadow">
            <div class="input-group-prepend">
                <span class="input-group-text" id="password">Пароль</span>
            </div>
            <input type="password" class="form-control" placeholder="Пароль" aria-label="password"
                aria-describedby="password" name="password">
        </div>
        <button type="submit" class="btn btn-dark shadow" name="do_login">Создать пароль</button>
    </div>
</form>

<?php
}
require_once "components/footer.php";
?>
