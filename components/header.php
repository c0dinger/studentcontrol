<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Scada&display=swap" rel="stylesheet">
    <style>

    * {
        
        font-family: 'Scada', sans-serif;
    }
    </style>
    <title>StudentControl</title>
</head>

<body class="m-5">
    <h1>StudentControl</h1>
    <span>Лучший помощник в <strong>обучении!</strong></span>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark mt-3 rounded-top">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="index">Главная <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="list">Работа с БД учеников</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="create_work">Работа с БД сотрудников</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="pass">Режим прохода</a>
                </li>
            </ul>
        </div>
    </nav>