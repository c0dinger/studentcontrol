<?php
require 'connections/rb.php';
if (isset($_POST['create_set'])) {
    $text = '<?php' . "\n". '$set_school = ' . "'" . $_POST['set_name'] . "';" . "\n" . '$set_email = ' . "'" . $_POST['set_email'] . "';" . "\n" . '$set_password = ' . "'" . $_POST['set_password'] . "';" . "\n" . '?>';
    $fo = fopen("configure.php", "w");
    fwrite($fo, $text);
    fclose($fo);
}
if (isset($_POST['create_bd'])) {

    // Подключение к MySQL
    $servername = "localhost"; // локалхост
    $username = "root"; // имя пользователя
    $password = ""; // пароль если существует

    // Создание соединения
    $conn = new mysqli($servername, $username, $password);
    // Проверка соединения
    if ($conn->connect_error) {
    die("Ошибка подключения: " . $conn->connect_error);
    }

    // Созданние базы данных
    $sql = "CREATE DATABASE studentcontrol";
    if ($conn->query($sql) === TRUE) {
        echo "<h1>База данных создана успешно</h1>";
    } else {
        echo "Ошибка создания базы данных: " . $conn->error;
    }
    $conn->close();

    R::setup('mysql:host=localhost;dbname=studentcontrol', 'root', '');
    $admin = R::dispense('admin');
    $admin->username = "admin";
    $admin->password = "admin";
    R::store($admin);

    $work = R::dispense('work');
    $work->name = "Удалите";
    $work->surname = "Удалите";
    $work->byear = "0000-00-00";
    $work->class = "Нет класс.рук.";
    $work->week = "0";
    $work->month = "0";
    $work->year = "0";
    $work->password = "admin";
    $work->email = "email@gmail.com";
    R::store($work);

}
?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Scada&display=swap" rel="stylesheet">
    <style>
    * {

        font-family: 'Scada', sans-serif;
    }
    </style>
    <title>StudentControl</title>
</head>

<div class="container m-5">
    <h1>Настройка StudentControl</h1>

    <h3 class="mt-5">Настройка конфигурации</h3>
    <form action="settings.php" method="POST">
        <div class="form-group">
            <label for="exampleInputEmail1">Название учебного заведения</label>
            <input type="school" class="form-control" id="school" aria-describedby="school"
                placeholder="Например: МБОУ-- №--" name="set_name">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Ваш E-mail адресс</label>
            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                placeholder="Например: email@gmail.com" name="set_email">
            <small id="emailHelp" class="form-text text-muted">Мы не получаем ваши данные! Все они остаются на вашем
                компьютере.</small>
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Пароль от почты</label>
            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Пароль"
                name="set_password">
        </div>
        <button class="btn btn-success" name="create_set">Создать файл настроек</button>
    </form>

    <form action="settings.php" method="POST" class="mt-5">
        <h3 class="text-danger">Операцию ниже выполнять 1 раз!!1</h3>
        <button class="btn btn-danger" name="create_bd">Создать базу данных</button>
    </form>
</div>