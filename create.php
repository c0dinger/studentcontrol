<?php
require_once "db.php";
require_once "components/header.php";
$data = $_POST;
if (isset($_SESSION["logged_user"]))
{

    if (isset($data["do_create"]))
    {
        $error = array();

        if ($data["class_name"] == '')
        {
            $error[] = "Введите номер класса";
        }
        if (empty($error))
        {

            $number = $data["class_name"];
            $class = R::dispense("$number");
            $class->id = "0";
            $class->name = "Это проверочная запись.";
            $class->surname = "Её можно удалить!";
            $class->byear = "";
            $class->class = "$number";
            $class->week = "";
            $class->month = "";
            $class->year = "";
            $id = R::store($class);
        }
        else
        {
            echo "<h3 class='bg-danger'>$error[0]</h3>";
        }
    }
?>
<div class="container">
    <div class="container bg-danger text-light">
        <div class="h2">Внимание!</div>
        <div class="h3">Для создания класса используйте маленькие латинские буквы! Например 7Б = 7b, а 7В = 7v
        </div>
    </div>
    <form action="create.php" method="POST" class="shadow rounded-bottom pb-3 bg-success pt-3">
        <div class="container">
            <h3 class="mt-5 text-light">Создание БД</h3>
            <div class="input-group mb-3 shadow">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="login">Номер класса</span>
                </div>
                <input type="text" class="form-control" placeholder="Например: 7B" aria-label="class_name"
                    aria-describedby="class_name" name="class_name">
            </div>

            <button type="submit" class="btn btn-dark shadow" name="do_create">Создать</button>
        </div>
    </form>

</div>

<? require_once "components/exit-button.php"; ?>
<?php
}
else
{ ?>
<?php
    if (isset($data['do_login']))
    {
        $error = array();
        $user = R::findOne('admin', 'username = ?', array(
            $data['login']
        ));
        if ($user)
        {
            if ($data['password'] = $user->password)
            {
                $_SESSION['logged_user'] = $user;
            }
            else
            {
                $error[] = "Пароль введён не верно!";
            }
        }
        else
        {
            $error[] = "Данного логина не существует";
        }
    }
    if (!empty($error))
    {
        echo $error[0];
    }
?>
<form action="index.php" method="POST" class="shadow rounded-bottom pb-3 bg-primary pt-3">
    <div class="container">
        <h3 class="mt-5 text-light">Вход в StudentControl</h3>
        <div class="input-group mb-3 shadow">
            <div class="input-group-prepend">
                <span class="input-group-text" id="login">Имя</span>
            </div>
            <input type="text" class="form-control" placeholder="Имя пользователя" aria-label="login"
                aria-describedby="login" name="login">
        </div>

        <div class="input-group mb-3 shadow">
            <div class="input-group-prepend">
                <span class="input-group-text" id="password">Пароль</span>
            </div>
            <input type="password" class="form-control" placeholder="Пароль" aria-label="password"
                aria-describedby="password" name="password">
        </div>
        <button type="submit" class="btn btn-dark shadow" name="do_login">Войти</button>
    </div>
</form>

<?php
}
require_once "components/footer.php";
?>
