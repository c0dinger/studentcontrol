<?php
require_once "db.php";
require_once "components/header.php";
$data = $_POST;

if (isset($_SESSION["logged_user"]))
{
    if (isset($data['do_update']))
    {
        $id_update = $data['id_update'];
        $week_update = $data['week_update'];
        $month_update = $data['month_update'];
        $year_update = $data['year_update'];
        R::exec("UPDATE work SET `week`='$week_update', `month`='$month_update', `year`='$year_update' WHERE `id` = $id_update");
    }

    if (isset($data['do_create']))
    {
        $work_create = R::dispense("work");
        $work_create->name = $data["name"];
        $work_create->surname = $data["surname"];
        $work_create->byear = $data["byear"];
        $work_create->class = $data["class"];
        $work_create->week = "0";
        $work_create->month = "0";
        $work_create->year = "0";
        $work_create->password = $data["password"];
        $work_create->email = $data["mail"];
        $student_store = R::store($work_create);
    }

    if (isset($data['do_delete']))
    {
        $class_delete = $data["class_num"];
        $class_num = R::dispense("work");
        $id = $data['id'];
        R::exec("DELETE FROM `work` WHERE `id` = '{$id}'");
    } ?>


<form action="create_work.php" method="POST">
    <div class="input-group mb-3 shadow mt-3">
        <div class="input-group-prepend">
            <span class="input-group-text" id="name">И</span>
        </div>
        <input type="text" class="form-control" placeholder="Виктор" aria-label="name" aria-describedby="name"
            name="name">

        <div class="input-group-prepend">
            <span class="input-group-text" id="surname">Ф</span>
        </div>
        <input type="text" class="form-control" placeholder="Цой" aria-label="surname" aria-describedby="surname"
            name="surname">

        <div class="input-group-prepend">
            <span class="input-group-text" id="byear">Г.р</span>
        </div>
        <input type="text" class="form-control" placeholder="1960-06-21" aria-label="byear" aria-describedby="byear"
            name="byear">

        <div class="input-group-prepend">
            <span class="input-group-text" id="class">Кл.</span>
        </div>
        <input type="text" class="form-control" placeholder="10b" aria-label="class" aria-describedby="class"
            name="class">
        <div class="input-group-prepend">
            <span class="input-group-text" id="mail">Почта</span>
        </div>
        <input type="text" class="form-control" placeholder="Почта директора" aria-label="mail" aria-describedby="mail"
            name="mail">
        <div class="input-group-prepend">
            <span class="input-group-text" id="password">Пароль</span>
        </div>
        <input type="text" class="form-control" placeholder="0" aria-label="password" aria-describedby="password"
            name="password">
    </div>

    <button type="submit" class="btn btn-success shadow" name="do_create">Создать</button>
</form>

<form action="" method="POST">
    <div class="input-group mb-3 shadow mt-3">
        <div class="input-group-prepend">
            <span class="input-group-text" id="id_update">ID</span>
        </div>
        <input type="text" class="form-control" placeholder="ID" aria-label="id_update" aria-describedby="id_update"
            name="id_update">

        <div class="input-group-prepend">
            <span class="input-group-text" id="week_update">П. в н.</span>
        </div>
        <input type="text" class="form-control" placeholder="0" aria-label="week_update" aria-describedby="week_update"
            name="week_update">

        <div class="input-group-prepend">
            <span class="input-group-text" id="month_update">П. в м.</span>
        </div>
        <input type="text" class="form-control" placeholder="0" aria-label="month_update"
            aria-describedby="month_update" name="month_update">

        <div class="input-group-prepend">
            <span class="input-group-text" id="year_update">П. в г.</span>
        </div>
        <input type="text" class="form-control" placeholder="0" aria-label="year_update" aria-describedby="year_update"
            name="year_update">
    </div>

    <button type="submit" class="btn btn-success shadow" name="do_update">Обновить</button>
</form>

<form action="#" method="POST">
    <div class="input-group mb-3 shadow mt-3">
        <div class="input-group-prepend">
            <span class="input-group-text" id="id">ID</span>
        </div>
        <input type="text" class="form-control" placeholder="Удалить id" aria-label="id" aria-describedby="id"
            name="id">

    </div>
    <button type="submit" class="btn btn-danger shadow" name="do_delete">Удалить</button>
</form>
<table class="table mt-5">
    <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Имя</th>
            <th scope="col">Фамилия</th>
            <th scope="col">Дата рождения</th>
            <th scope="col">Класс</th>
            <th scope="col">Неделя</th>
            <th scope="col">Месяц</th>
            <th scope="col">Год</th>
        </tr>
    </thead>
    <tbody>
        <?php
    $classes = R::getAll("SELECT * FROM `work`");
    foreach ($classes as $class1)
    { ?>
        <tr>
            <th scope="row"><?php echo $class1["id"] ?></th>
            <td><?php echo $class1["name"] ?></td>
            <td><?php echo $class1["surname"] ?></td>
            <td><?php echo $class1["byear"] ?></td>
            <td><?php echo $class1["class"] ?></td>
            <td><?php echo $class1["week"] ?></td>
            <td><?php echo $class1["month"] ?></td>
            <td><?php echo $class1["year"] ?></td>
        </tr>
        <?php
    } ?>
    </tbody>
</table>



<? require_once "components/exit-button.php"; ?>

<?php
}
else
{ ?>
<?php
    if (isset($data['do_login']))
    {
        $error = array();
        $user = R::findOne('admin', 'username = ?', array(
            $data['login']
        ));
        if ($user)
        {
            if ($data['password'] = $user->password)
            {
                $_SESSION['logged_user'] = $user;
            }
            else
            {
                $error[] = "Пароль введён не верно!";
            }
        }
        else
        {
            $error[] = "Данного логина не существует";
        }
    }
    if (!empty($error))
    {
        echo $error[0];
    }
?>
<form action="index.php" method="POST" class="shadow rounded-bottom pb-3 bg-primary pt-3">
    <div class="container">
        <h3 class="mt-5 text-light">Вход в StudentControl</h3>
        <div class="input-group mb-3 shadow">
            <div class="input-group-prepend">
                <span class="input-group-text" id="login">Имя</span>
            </div>
            <input type="text" class="form-control" placeholder="Имя пользователя" aria-label="login"
                aria-describedby="login" name="login">
        </div>

        <div class="input-group mb-3 shadow">
            <div class="input-group-prepend">
                <span class="input-group-text" id="password">Пароль</span>
            </div>
            <input type="password" class="form-control" placeholder="Пароль" aria-label="password"
                aria-describedby="password" name="password">
        </div>
        <button type="submit" class="btn btn-dark shadow" name="do_login">Войти</button>
    </div>
</form>

<?php
}
require_once "components/footer.php";
?>
