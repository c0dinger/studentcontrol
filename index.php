<?php
require_once "db.php";
require_once "components/header.php";
$data = $_POST;
if (isset($_SESSION['logged_user']))
{ ?>
<div class="row text-center">
    <div class="col">
        <form class="bg-danger pt-3 pb-4 rounded-bottom text-light shadow-lg">
            <h1 class="shadow pb-4">1. Задайте пароль для доступа</h1>
            <button type="button" class="btn btn-dark mt-3"><a href="create_password"
                    class="text-light">Задать</a></button>
        </form>
    </div>
    <div class="col">
        <form class="bg-primary pt-3 pb-4 rounded-bottom text-light shadow-lg">
            <h1 class="shadow pb-4">2. Создайте свою первую базу данных учеников</h1>
            <button type="button" class="btn btn-dark mt-3"><a href="create"
                    class="text-light">Создать БД</a></button>
        </form>
    </div>
    <div class="col">
    <form class="bg-success pt-3 pb-4 rounded-bottom text-light shadow-lg">
            <h1 class="shadow pb-4">3. Работа с базой сотрудников</h1>
            <button type="button" class="btn btn-dark mt-3"><a href="create_work"
                    class="text-light">База данных</a></button>
        </form>
    </div>
</div>
<?php require_once "components/exit-button.php";
}
else
{

    if (isset($data['do_login']))
    {
        $error = array();
        $user = R::findOne('admin', 'username = ?', array(
            $data['login']
        ));
        if ($user)
        {
            if ($data['password'] = $user->password)
            {
                $_SESSION['logged_user'] = $user;
            }
            else
            {
                $error[] = "Пароль введён не верно!";
            }
        }
        else
        {
            $error[] = "Данного логина не существует";
        }
    }
    if (!empty($error))
    {
        echo $error[0];
    }
?>
<form action="index.php" method="POST" class="shadow rounded-bottom pb-3 bg-primary pt-3">
    <div class="container">
        <h3 class="mt-5 text-light">Вход в StudentControl</h3>
        <div class="input-group mb-3 shadow">
            <div class="input-group-prepend">
                <span class="input-group-text" id="login">Имя</span>
            </div>
            <input type="text" class="form-control" placeholder="Имя пользователя" aria-label="login"
                aria-describedby="login" name="login">
        </div>

        <div class="input-group mb-3 shadow">
            <div class="input-group-prepend">
                <span class="input-group-text" id="password">Пароль</span>
            </div>
            <input type="password" class="form-control" placeholder="Пароль" aria-label="password"
                aria-describedby="password" name="password">
        </div>
        <button type="submit" class="btn btn-dark shadow" name="do_login">Войти</button>
    </div>
</form>

<?php
}
require_once "components/footer.php";
?>
