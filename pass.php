<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require 'vendor/autoload.php';
require_once "db.php"; ?>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Scada&display=swap" rel="stylesheet">
    <style>
    * {

        font-family: 'Scada', sans-serif;
    }
    </style>
    <title>StudentControl</title>
</head>
<?php $data = $_POST; ?>

<div class="bg-danger pt-4 pb-3">
    <div class="container mt-5 text-light">
        <h1>StudentControl</h1>
        <span>Лучший помощник в <strong>обучении!</strong></span>
    </div>
</div>


<form action="pass.php" method="POST">
    <div class="container mt-5">
        <div class="row">
            <div class="col">
                <h1>Выберите ваш класс</h1>
                <?php $class = R::getAll('show tables from studentcontrol;'); ?>
                <select class="browser-default custom-select shadow" name="class">
                    <?php
                    foreach ($class as $key => $class_key)
                    {
                        foreach ($class_key as $inner_key)
                        {
                            if ($inner_key == "admin")
                            {
                                continue;
                            }
                            else
                            { ?>
                                <option value="<?php echo $inner_key ?>"><?php echo $inner_key ?></option>
                            <?php
                            }
                        }
                    } ?>
                </select>
            </div>
            <div class="col">
                <h1>Введите ваш ID</h1>
                <div class="input-group shadow">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="id">ID</span>
                    </div>
                    <input type="text" class="form-control" placeholder="id" aria-label="id" aria-describedby="id"
                        name="id">
                </div>
            </div>
            <div class="col">
                <h1>Введите ваш пароль</h1>
                <div class="input-group shadow">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="password">Пароль</span>
                    </div>
                    <input type="password" class="form-control" placeholder="Пароль" aria-label="password"
                        aria-describedby="password" name="password">
                </div>
            </div>
            <div class="col">
                <input type="submit" value="Авторизоваться" name="do_auth" class="btn btn-success mt-5">
            </div>
        </div>
    </div>
</form>


<?php
if (isset($data['do_auth']))
{
    $error = array();
    $selected_class = $data['class'];
    $id = $data['id'];
    $password = $data['password'];
    $user = R::findOne("$selected_class", 'id = ?', array(
        $data['id']
    ));
    if ($user)
    {
        if ($password == $user["password"])
        {
            R::exec("UPDATE {$selected_class} SET week=week + 1 WHERE id = {$id}");
            R::exec("UPDATE {$selected_class} SET month=month + 1 WHERE id = {$id}");
            R::exec("UPDATE {$selected_class} SET year=year + 1 WHERE id = {$id}");

            $name = $user["name"];
            $surname = $user["surname"];
            $email = $user["email"];

            // Import PHPMailer classes into the global namespace
            // These must be at the top of your script, not inside a function
            

            // Instantiation and passing `true` enables exceptions
            $mail = new PHPMailer(true);
            //Tell PHPMailer to use SMTP
            $mail->isSMTP();
            $mail->CharSet = 'utf-8';
            //Enable SMTP debugging
            // 0 = off (for production use)
            // 1 = client messages
            // 2 = client and server messages
            $mail->SMTPDebug = 2;
            //Set the hostname of the mail server
            $mail->Host = 'smtp.gmail.com';
            // use
            // $mail->Host = gethostbyname('smtp.gmail.com');
            // if your network does not support SMTP over IPv6
            //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
            $mail->Port = 587;
            //Set the encryption system to use - ssl (deprecated) or tls
            $mail->SMTPSecure = 'tls';
            //Whether to use SMTP authentication
            $mail->SMTPAuth = true;
            //Username to use for SMTP authentication - use full email address for gmail
            $mail->Username = "Ваша почта";
            //Password to use for SMTP authentication
            $mail->Password = "Пароль от почты";
            //Set who the message is to be sent from
            $mail->setFrom('Ваша почта', 'МБОУ - ваше заведение');
            //Set an alternative reply-to address
            $mail->addReplyTo('Ваша почта', 'МБОУ - ваше заведение');
            //Set who the message is to be sent to
            $mail->addAddress("$email", "$");
            //Set the subject line
            $mail->Subject = 'Посещение школы №--';
            //Read an HTML message body from an external file, convert referenced images to embedded,
            //convert HTML into a basic plain-text alternative body
            $week = $user["week"];
            $month = $user["month"];
            $year = $user["year"];
            $mail->Body = "$name $surname посетил(а) школу!\nНеделя: $week\nМесяц: $month\nГод: $year";
            //Replace the plain text body with one created manually
            $mail->AltBody = 'This is a plain-text message body';
            if (!$mail->send())
            {
                echo "Mailer Error: " . $mail->ErrorInfo;
            }
            else
            { ?>
                <div class="container">
                    <h1 class="text-success">Можете проходить -<br>Для продолжения <a href="pass">нажмите сюда</a></h1>
                </div>
<?php
            }
            //Section 2: IMAP
            //IMAP commands requires the PHP IMAP Extension, found at: https://php.net/manual/en/imap.setup.php
            //Function to call which uses the PHP imap_*() functions to save messages: https://php.net/manual/en/book.imap.php
            //You can use imap_getmailboxes($imapStream, '/imap/ssl', '*' ) to get a list of available folders or labels, this can
            //be useful if you are trying to get this working on a non-Gmail IMAP server.
            function save_mail($mail)
            {
                //You can change 'Sent Mail' to any other folder or tag
                $path = "{imap.gmail.com:993/imap/ssl}[Gmail]/Sent Mail";
                //Tell your server to open an IMAP connection using the same username and password as you used for SMTP
                $imapStream = imap_open($path, $mail->Username, $mail->Password);
                $result = imap_append($imapStream, $path, $mail->getSentMIMEMessage());
                imap_close($imapStream);
                return $result;
            }

        }
        else
        {
            $error[] = "<div class='container'>
            <h1 class='text-danger'>Пароль не верен</a></h1>
        </div>";
        }
    }
    else
    {
        $error[] = "<div class='container'><h1 class='text-danger'>ID не найден</a></h1></div>";
    }
}
if (!empty($error))
{
    echo $error[0];
}

?>

<div class="mt-5 bg-light">
    <hr>
</div>
<div class="mt-5 bg-light">
    <hr>
</div>
<div class="mt-5 bg-light">
    <hr>
</div>
<div class="mt-5 bg-light">
    <hr>
</div>
<div class="mt-5 bg-light">
    <hr>
</div>

<form action="pass.php" method="POST" class="shadow rounded-bottom pb-5 bg-primary pt-3">
    <div class="container">
        <h3 class="mt-5 text-light">Выход из программы "Пропуск"</h3>
        <div class="input-group mb-3 shadow">
            <div class="input-group-prepend">
                <span class="input-group-text" id="login">Администратор</span>
            </div>
            <input type="text" class="form-control" placeholder="Логин администратора" aria-label="login"
                aria-describedby="login" name="login">
        </div>

        <div class="input-group mb-3 shadow">
            <div class="input-group-prepend">
                <span class="input-group-text" id="password_exit">Пароль</span>
            </div>
            <input type="password" class="form-control" placeholder="Пароль" aria-label="password_exit"
                aria-describedby="password_exit" name="password_exit">
        </div>
        <button type="submit" class="btn btn-danger shadow" name="do_leave">Выйти</button>
    </div>
</form>

<?php
if (isset($data['do_leave']))
{
    $error = array();
    $password = $data['password_exit'];
    $user = R::findOne('admin', 'username = ?', array(
        $data['login']
    ));
    if ($user)
    {
        if ($password == $user["password"])
        { ?>
            <a href="index">Выйти</a>
    <?php
        }
        else
        {
            $error[] = "<div class='container'>
            <h1 class='text-danger'>Пароль не верен - отправка на попытки взлома на сервер</a></h1></div>";
        }
    }
    else
    {
        $error[] = "<div class='container'><h1 class='text-danger'>Логин не найден</h1></div>";
    }
}
if (!empty($error))
{
    echo $error[0];
}

?>