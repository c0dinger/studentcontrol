<?php
require_once "db.php";
require_once "components/header.php";
$data = $_POST;
if (isset($_SESSION["logged_user"]))
{
    $exec = R::getAssoc("SHOW TABLES FROM studentcontrol"); 
    if (isset($data['do_update']))
    {
        $id_update = $data['id_update'];
        $week_update = $data['week_update'];
        $month_update = $data['month_update'];
        $year_update = $data['year_update'];
        $class_update = $data['class_up'];
        R::exec("UPDATE $class_update SET `week`='$week_update', `month`='$month_update', `year`='$year_update' WHERE `id` = $id_update");
    }

    if (isset($data['do_create']))
    {
        $class_create = $data["class_cr"];
        $student_create = R::dispense("$class_create");
        $student_create->name = $data["name"];
        $student_create->surname = $data["surname"];
        $student_create->byear = $data["byear"];
        $student_create->class = $data["class_cr"];
        $student_create->week = "0";
        $student_create->month = "0";
        $student_create->year = "0";
        $student_create->password = $data["password"];
        $student_create->email = $data["email"];
        $student_store = R::store($student_create);
    }
    if (isset($data['do_delete']))
    {
        $class_delete = $data["class_dl"];
        $class_num = R::dispense("$class_delete");
        $id = $data['id'];
        R::exec("DELETE FROM `$class_delete` WHERE `id` = '{$id}'");
    } ?>

<div class="container">
    <form action="list.php" method="POST">
        <h1>Выберите ваш класс</h1>
        <?php $class = R::getAll('show tables from studentcontrol;'); ?>
        <select class="browser-default custom-select shadow" name="class">
            <?php
        foreach ($class as $key => $class_key)
        {
            foreach ($class_key as $inner_key)
            {
                if ($inner_key == "admin" or $inner_key == "work")
                {
                    continue;
                }
                else
                { ?>
                    <option value="<?php echo $inner_key ?>"><?php echo $inner_key ?></option>
                <?php
                }
            }
        } ?>
        </select>
        <input type="submit" name="submit" value="Просмотр" class="btn btn-success mt-3" />
    </form>
    <?php if (isset($_POST['submit']))
    {
        $selected_class = $data['class'];
        $classes = R::getAll("SELECT * FROM `$selected_class`");
?>
    <form action="list.php" method="POST">
        <div class="input-group mb-3 shadow mt-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="name">И</span>
            </div>
            <input type="text" class="form-control" placeholder="Виктор" aria-label="name" aria-describedby="name"
                name="name">

            <div class="input-group-prepend">
                <span class="input-group-text" id="surname">Ф</span>
            </div>
            <input type="text" class="form-control" placeholder="Цой" aria-label="surname" aria-describedby="surname"
                name="surname">

            <div class="input-group-prepend">
                <span class="input-group-text" id="byear">Г.р</span>
            </div>
            <input type="text" class="form-control" placeholder="1960-06-21" aria-label="byear" aria-describedby="byear"
                name="byear">
            <div class="input-group-prepend">
                <span class="input-group-text" id="password">Пароль</span>
            </div>
            <input type="text" class="form-control" placeholder="0" aria-label="password" aria-describedby="password"
                name="password">
            <div class="input-group-prepend">
                <span class="input-group-text" id="email">Почта</span>
            </div>
            <input type="email" class="form-control" placeholder="0" aria-label="email" aria-describedby="email"
                name="email">
            <div class="input-group-prepend">
                <span class="input-group-text" id="email">Класс</span>
            </div>
            <?php $class_cr = R::getAll('show tables from studentcontrol;'); ?>
            <select class="browser-default custom-select shadow" name="class_cr">
                <?php
        foreach ($class_cr as $key => $class_key_cr)
        {
            foreach ($class_key_cr as $inner_key_cr)
            {
                if ($inner_key_cr == "admin" or $inner_key_cr == "work")
                {
                    continue;
                }
                else
                { ?>
                <option value="<?php echo $inner_key_cr ?>"><?php echo $inner_key_cr ?></option>
                <?php
                }
            }
        } ?>
            </select>
        </div>
        <button type="submit" class="btn btn-success shadow" name="do_create">Создать</button>
    </form>



    <form action="" method="POST">
        <div class="input-group mb-3 shadow mt-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="id_update">ID</span>
            </div>
            <input type="text" class="form-control" placeholder="ID" aria-label="id_update" aria-describedby="id_update"
                name="id_update">

            <div class="input-group-prepend">
                <span class="input-group-text" id="class_update">Кл.</span>
            </div>
            <?php $class_up = R::getAll('show tables from studentcontrol;'); ?>
            <select class="browser-default custom-select shadow" name="class_up">
                <?php 
        foreach ($class_up as $key_up => $class_key_up)
        {
            foreach ($class_key_up as $inner_key_up)
            {
                if ($inner_key_up == "admin" or $inner_key_up == "work")
                {
                    continue;
                }
                else
                { ?>
                <option value="<?php echo $inner_key_up ?>"><?php echo $inner_key_up ?>
                </option>
                <?php
                }
            }
        } ?>
            </select>

            <div class="input-group-prepend">
                <span class="input-group-text" id="week_update">П. в н.</span>
            </div>
            <input type="text" class="form-control" placeholder="0" aria-label="week_update"
                aria-describedby="week_update" name="week_update">

            <div class="input-group-prepend">
                <span class="input-group-text" id="month_update">П. в м.</span>
            </div>
            <input type="text" class="form-control" placeholder="0" aria-label="month_update"
                aria-describedby="month_update" name="month_update">

            <div class="input-group-prepend">
                <span class="input-group-text" id="year_update">П. в г.</span>
            </div>
            <input type="text" class="form-control" placeholder="0" aria-label="year_update"
                aria-describedby="year_update" name="year_update">
        </div>

        <button type="submit" class="btn btn-success shadow" name="do_update">Обновить</button>
    </form>

    <form action="#" method="POST">
        <div class="input-group mb-3 shadow mt-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="id">ID</span>
            </div>
            <input type="text" class="form-control" placeholder="Удалить id" aria-label="id" aria-describedby="id"
                name="id">
            <div class="input-group-prepend">
                <span class="input-group-text" id="email">Класс</span>
            </div>
            <?php $class_dl = R::getAll('show tables from studentcontrol;'); ?>
            <select class="browser-default custom-select shadow" name="class_dl">
                <?php
        foreach ($class_dl as $key_dl => $class_key_dl)
        {
            foreach ($class_key_dl as $inner_key_dl)
            {
                if ($inner_key_dl == "admin" or $inner_key_dl == "work")
                {
                    continue;
                }
                else
                { ?>
                <option value="<?php echo $inner_key_dl ?>"><?php echo $inner_key_dl ?>
                </option>
                <?php
                }
            }
        } ?>
            </select>
        </div>
        <button type="submit" class="btn btn-danger shadow" name="do_delete">Удалить</button>
    </form>
    <table class="table mt-5">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Имя</th>
                <th scope="col">Фамилия</th>
                <th scope="col">Дата рождения</th>
                <th scope="col">Класс</th>
                <th scope="col">Неделя</th>
                <th scope="col">Месяц</th>
                <th scope="col">Год</th>
                <th scope="col">Почта</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($classes as $class1)
        { ?>
            <tr>
                <th scope="row"><?php echo $class1["id"] ?></th>
                <td><?php echo $class1["name"] ?></td>
                <td><?php echo $class1["surname"] ?></td>
                <td><?php echo $class1["byear"] ?></td>
                <td><?php echo $class1["class"] ?></td>
                <td><?php echo $class1["week"] ?></td>
                <td><?php echo $class1["month"] ?></td>
                <td><?php echo $class1["year"] ?></td>
                <td><?php echo $class1["email"] ?></td>
            </tr>
            <?php
        }
    } ?>
        </tbody>
    </table>


</div>
<? require_once "components/exit-button.php"; ?>

<?php
}
else
{ ?>
<?php
    if (isset($data['do_login']))
    {
        $error = array();
        $user = R::findOne('admin', 'username = ?', array(
            $data['login']
        ));
        if ($user)
        {
            if ($data['password'] = $user->password)
            {
                $_SESSION['logged_user'] = $user;
            }
            else
            {
                $error[] = "Пароль введён не верно!";
            }
        }
        else
        {
            $error[] = "Данного логина не существует";
        }
    }
    if (!empty($error))
    {
        echo $error[0];
    }
?>
<form action="index.php" method="POST" class="shadow rounded-bottom pb-3 bg-primary pt-3">
    <div class="container">
        <h3 class="mt-5 text-light">Вход в StudentControl</h3>
        <div class="input-group mb-3 shadow">
            <div class="input-group-prepend">
                <span class="input-group-text" id="login">Имя</span>
            </div>
            <input type="text" class="form-control" placeholder="Имя пользователя" aria-label="login"
                aria-describedby="login" name="login">
        </div>

        <div class="input-group mb-3 shadow">
            <div class="input-group-prepend">
                <span class="input-group-text" id="password">Пароль</span>
            </div>
            <input type="password" class="form-control" placeholder="Пароль" aria-label="password"
                aria-describedby="password" name="password">
        </div>
        <button type="submit" class="btn btn-dark shadow" name="do_login">Войти</button>
    </div>
</form>

<?php
}
?>
<?php
require_once "components/footer.php";
?>
